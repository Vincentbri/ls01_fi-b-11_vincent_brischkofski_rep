
import java.util.Scanner;

class FahrkartenautomatMitMethodenalt
{
    public static void main(String[] args)
    {
      
       double ticketbetrag;
       double rückgabebetrag;
       Scanner tastatur = new Scanner(System.in);
       
       ticketbetrag = fahrkartenbestellungErfassen(tastatur);
       rückgabebetrag = fahrkartenBezahlen(tastatur,ticketbetrag);
       fahrkartenAusgeben(rückgabebetrag);
	   tastatur.close();
       
    }
    
    public static double fahrkartenbestellungErfassen(Scanner tastatur) {//Methode zur erfassung des Ticketpreises und Anzahl
    	
    	System.out.print("Ticketpreis (EURO): ");
    	double ticketpreis = tastatur.nextDouble();

        System.out.println("Anzahl der Tickets:");
        double tickets = tastatur.nextDouble();
        
        double fahrkartenbestellungErfassen = ticketpreis * tickets;
        return fahrkartenbestellungErfassen;
      

    }
    
    public static double fahrkartenBezahlen(Scanner tastatur, double zuZahlen) {//Methode zur bezahlung der fahrkarten
    	 double eingezahlterGesamtbetrag = 0.0;
    	   while(eingezahlterGesamtbetrag < zuZahlen)
           {
        	   System.out.printf("Noch zu zahlen: %.2f  Euro \n" , (zuZahlen - eingezahlterGesamtbetrag ));
        	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
        	   double eingeworfeneMünze = tastatur.nextDouble();
               eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMünze;
           }
    	   double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
    	   return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben(double rückgabebetrag) {//Methode für die ausgabe der Fahrkarten
    	System.out.println("\nFahrschein wird ausgegeben");
    	
    	warte(10);
       System.out.println("\n\n");
       rueckgeldAusgeben(rückgabebetrag);
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    		   			  "vor Fahrtantritt entwerten zu lassen!\n"+
               			  "Wir wünschen Ihnen eine gute Fahrt.");
    
    }
    public static void rueckgeldAusgeben( double rückgabebetrag) {//Methode für die Berechnung des rueckgelds
    	String einheit = "Euro";
    	
        if(rückgabebetrag > 0.0)
        	
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro \n" ,rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
            	muenzeAusgeben(5,einheit);
  	          rückgabebetrag -= 0.05;
            }
        }
        
    }
        
       public static void warte(int millisekunde) {//Methode für die Zeit verzögerung
        	for (int i = 0; i < millisekunde; i++)          	   
             {
                System.out.print("=");
                try {
      			Thread.sleep(250);
      		} catch (InterruptedException e) {
      			// TODO Auto-generated catch block
     			e.printStackTrace();
      		}
            }
    	
    		}
        public static void muenzeAusgeben(int betrag, String einheit) {// Methode für die Münzausgabe
        	if(betrag >=5) {
        		einheit = "CENT";
        		System.out.println(betrag+" "+einheit);
        		
        	}else System.out.println(betrag+" "+einheit);
            
        }
}

    