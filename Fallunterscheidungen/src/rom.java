import java.util.Scanner;

public class rom{
    public static void main(String []args) {
        Scanner test=new Scanner(System.in);
       
        
        System.out.println("Geben Sie ein römisches Zeichen (I, V, X, L, C, D, M) ein: ");
        char zeichen= test.next().charAt(0);

        switch(zeichen){
            case 'I':
                System.out.println("1");
                break;
            case 'V':
                System.out.println("5");
                break;
            case 'X':
                System.out.println("10");
                break;
            case 'L':
                System.out.println("50");
                break;
            case 'C':
                System.out.println("100");
                break;
            case 'D':
                System.out.println("500");
                break;
            case 'M':
                System.out.println("1000");
                break;
            default:
                System.out.println("Falsche Eingabe");
                break;
                
              
               
        }
    }
}