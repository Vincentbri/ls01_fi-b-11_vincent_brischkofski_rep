import java.util.Scanner;
public class mittelwertMitMethoden {

	public static void main(String[] args) {
		//Deklaration von Variablen
				double zahl;
				double m;
				int anzahl;
				double sum = 0;
				
				Scanner myScanner = new Scanner (System.in);
				System.out.println("Dieses Programm berechnet den Mittelwert mehrerer Zahlen.");
				System.out.println("Bitte geben Sie die Anzahl der Zahlen ein.");
				anzahl = myScanner.nextInt();
				
				for(int i = 1; i <= anzahl; i++){
					System.out.println("Bitte geben Sie "+i+ ". Zahl ein: ");
					zahl = myScanner.nextDouble();
					sum = sum + zahl;
				}
				
				m = berechneMittelwert(anzahl,sum);
				
				ausgabe(m);	
				
				myScanner.close();

				


	}
	
	public static double eingabe(Scanner ms,String text) {
		System.out.println(text);
		double zahl = ms.nextDouble();
		return zahl;

		
	}
	
	public static double berechneMittelwert(int anzahl, double sum) {
		double m = sum/anzahl;
		return m;
		
		
	}
	public static void ausgabe(double mittelwert) {
		System.out.println("Mittelwert: " + mittelwert);


		
	}
	
}
