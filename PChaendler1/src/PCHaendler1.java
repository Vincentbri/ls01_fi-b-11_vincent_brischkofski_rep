import java.util.Scanner;

public class PCHaendler1 {
	
	public static void main(String[] args) {
		
		String artikel;
		int anzahl;
		double preis;
		double mwst;
		double nettogesamtpreis;
		double bruttogesamtpreis;
		
		
		
		Scanner myScanner = new Scanner(System.in);
		artikel = liesString(myScanner,"was m�chten Sie bestellen?");
		anzahl = liesInt(myScanner,"Geben SIe die Anzahl ein");
		preis = liesDouble(myScanner,"Geben Sie den Nettopreis ein");
		mwst= liesDouble(myScanner,"Geben Sie den Mehrwerststeuersatz in Prozent ein");
		nettogesamtpreis = berechneGesamtnettopreis(anzahl,preis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis,mwst);
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		myScanner.close();
		
	}
	public static String liesString(Scanner ms,String text) {
		System.out.println(text);	
		String artikel = ms.next();
		return artikel;
		
	}
	public static int liesInt(Scanner ms,String text) {
		System.out.println(text);
		int anzahl = ms.nextInt();
		return anzahl;
		
	}
	public static double liesDouble(Scanner ms,String text) {
		System.out.println(text);
		double preis = ms.nextDouble();
		return preis;
	}
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = (anzahl * preis);
		return nettogesamtpreis;
		
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis,double mwst) {
		double bruttogesamtpreis =(nettogesamtpreis*(1 + mwst / 100));
		return bruttogesamtpreis;
		
	}
	public static void rechnungausgeben(String artikel, int anzahl, double
			nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
