
public class Maingame {

	public static void main(String[] args) {
		// erzeugen der Objekte
		Raumschiff klingonen = new Raumschiff("IKS Heghta", 100.0, 100.0, 100.0, 100.0, 1, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100.0, 100.0, 100.0, 100.0, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Nivar", 80.0, 80.0, 100.0, 50.0, 0, 5);

		Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladung2 = new Ladung("Borg-Schrott", 5);
		Ladung ladung3 = new Ladung("Rote Materie", 2);
		Ladung ladung4 = new Ladung("Forschungssonde", 35);
		Ladung ladung5 = new Ladung("Batleth Klingonen Schwert", 200);
		Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
		Ladung ladung7 = new Ladung("Photonentorpedos", 3);

		// Ladungen werden den Raumschiffen hinzugefügt
		klingonen.addLadung(ladung1);
		klingonen.addLadung(ladung5);

		romulaner.addLadung(ladung2);
		romulaner.addLadung(ladung3);
		romulaner.addLadung(ladung6);

		vulkanier.addLadung(ladung7);
		vulkanier.addLadung(ladung4);
		
		// Klingonen schießen mit Photonen Torpedos auf die Romulaner
		klingonen.photonenTorpedosSchießen(romulaner);
		
		// Die Romulaner schießen mit der Phaserkanone auf die Klingonen
		romulaner.phaserKanonenSchießen(klingonen);
		
		// Vulkanier senden eine Nachricht an alle 
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		
		//Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein.
		
		vulkanier.ReperaturAndroidenEinsetzen(true, true, true, 2);
		
		//Die Vulkanier verladen Ihre Ladung “Photonentorpedos” in die Torpedoröhren Ihres Raumschiffes und räumen das Ladungsverzeichnis auf. 
		vulkanier.ladeTorpedo(3);
		vulkanier.Ladungaufräumen();
		
		//Die Klingonen rufen den Zustand des Raumschiffes und das Ladungsverzeichnis ab
		klingonen.zustandRaumschiff();
		klingonen.zeigeLadung();
		
		//Klingonen schießen 2 mal auf die romulaner
		klingonen.photonenTorpedosSchießen(romulaner);
		klingonen.photonenTorpedosSchießen(romulaner);
		
		//ALle rufen den Zustand des Raumschiffes ab und das Ladungsverzeichnis
		klingonen.zustandRaumschiff();
		klingonen.zeigeLadung();
		romulaner.zustandRaumschiff();
		romulaner.zeigeLadung();
		vulkanier.zustandRaumschiff();
		vulkanier.zeigeLadung();
		
		System.out.println(Raumschiff.schreibelogbuch());
		
	}

}
