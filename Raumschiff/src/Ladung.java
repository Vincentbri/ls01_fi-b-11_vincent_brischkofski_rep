
public class Ladung {

// Attribute
	private String name;
	private int anzahl;

	// Konstruktoren
	public Ladung() {// parameterlos

	}

	public Ladung(String name, int anzahl) { // voll parametisiert
		this.name = name;
		this.anzahl = anzahl;
	}

	// Getter und Setter Methoden
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	@Override
	public String toString() {
		return this.name+ " "+ this.anzahl;
	}

}
