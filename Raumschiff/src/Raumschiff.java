import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

	// Attribute
	private String name;
	private double energieversorgung;
	private double schild;
	private double lebenserhaltungssysteme;
	private double hülle;
	private int anzahlPTorpedos;
	private int anzahlAndroiden;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();
	public static ArrayList<String> broadcastCommunicator = new ArrayList<>();

	// Konstruktor

	public Raumschiff() {

	}

	public Raumschiff(String name, double energieversorgung, double schild, double lebenserhaltungssysteme,
			double hülle, int anzahlPTorpedos, int anzahlAndroiden) {
		super();
		this.name = name;
		this.energieversorgung = energieversorgung;
		this.schild = schild;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.hülle = hülle;
		this.anzahlPTorpedos = anzahlPTorpedos;
		this.anzahlAndroiden = anzahlAndroiden;
	}

	public void addLadung(Ladung ladung) {
		ladungsverzeichnis.add(ladung);
	}

	public void zustandRaumschiff() {
		System.out.println("\nDie Zustände des Raumschiffes " + this.name + " sind wie folgt: ");
		System.out.println("Energieversorgung: " + this.energieversorgung);
		System.out.println("Schild: " + this.schild);
		System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssysteme);
		System.out.println("Hülle: " + this.hülle);
		System.out.println("Anzahl der Photonen Torpedos: " + this.anzahlPTorpedos);
		System.out.println("Anzahl der Reperatur Droiden: " + this.anzahlAndroiden);
	}

	public void zeigeLadung() {
		for(int i = 0;i > ladungsverzeichnis.size(); i++) {
		System.out.println("Die Ladungen des Raumschiffes" + this.name + "sind wie folgt: ");
		System.out.println("Name der Ladung" + this.name);
		System.out.println("Anzahl der Ladung" + i);
		}

	}
	
	public void photonenTorpedosSchießen(Raumschiff ziel) {
		if (anzahlPTorpedos <= 0) {
			System.out.println("-=*Click*=-");
			broadcastCommunicator.add("-=*Click*=-");	
		}
			else {
				anzahlPTorpedos = anzahlPTorpedos -1 ;
				System.out.println("Photonentorpedo abgeschossen");
				broadcastCommunicator.add("Photonentorpedo abgeschossen ");
				trefferRaumschiff(ziel);
			}
		}
	public void phaserKanonenSchießen(Raumschiff ziel) {
		if (energieversorgung < 50 ) {
			System.out.println("-=*Click*=-");
			broadcastCommunicator.add("-=*Click*=-");
		}
		
			else {
				energieversorgung = energieversorgung - 50;
				System.out.println("Phaserkannone abgeschossen ");
				broadcastCommunicator.add("Phaserkannone abgeschossen");
				trefferRaumschiff(ziel);
		}
	}

	public void trefferRaumschiff(Raumschiff ziel) {
		schild = schild - 50;
		if(schild <= 0) {
			hülle = hülle - 50;
			energieversorgung = energieversorgung - 50;
			if(hülle == 0) {
				System.out.println("Alle Lebenserhaltungssysteme wurden von Raumschiff "+ ziel.name +" vernichtet \n");
				broadcastCommunicator.add("Alle Lebenserhaltungssysteme wurden von Raumschiff "+ ziel.name +" vernichtet \n");
			}

		}	
	}
	
	public void ladeTorpedo(int zähleTorpedo) {
		boolean Torpedovorhanden = false;
		
		for(int i = 0; i < ladungsverzeichnis.size(); i++) {
			if (ladungsverzeichnis.get(i).getName().equals("Photonentorpedo")) {
				Torpedovorhanden = true;
				int zähleTorpedos = ladungsverzeichnis.get(i).getAnzahl();
				if(zähleTorpedo >= ladungsverzeichnis.get(i).getAnzahl()) {
					this.setAnzahlPTorpedos(this.getAnzahlPTorpedos() + zähleTorpedos);
					ladungsverzeichnis.get(i).setAnzahl(0);
					System.out.println(zähleTorpedos + " photonentorpedos wurden genutzt \n");
				}else {
					this.setAnzahlPTorpedos(this.getAnzahlPTorpedos() + zähleTorpedo);
					ladungsverzeichnis.get(i).setAnzahl(ladungsverzeichnis.get(i).getAnzahl() - zähleTorpedo);
					System.out.println(zähleTorpedo + " photonentorpedos wurden genutzt \n");
				}
			}
		}
		if(!Torpedovorhanden) {
			System.out.println("kein Torpedo gefunden \n");
			broadcastCommunicator.add("-=*Click*=-");
		}
	}
	
	public void Ladungaufräumen() {
		for(int i = 0; i < ladungsverzeichnis.size(); i++) {
			if (ladungsverzeichnis.get(i).getAnzahl() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}
	
	public void ReperaturAndroidenEinsetzen(boolean energieversorgung, boolean schild, boolean hülle, int zähleAndroiden) {
		Random r = new Random();
		int amountTrues = 0;
		int randomInt = r.nextInt(100) + 1;
		if (zähleAndroiden > this.anzahlAndroiden) {
			zähleAndroiden = this.anzahlAndroiden;
		}
		
		if(energieversorgung) {
			amountTrues++;
		}
		if(schild) {
			amountTrues++;
		}
		if(hülle) {
			amountTrues++;
		}
		
		double repair = randomInt*zähleAndroiden/amountTrues;
		
		if(energieversorgung) {
			this.energieversorgung += repair;
		}
		if(schild) {
			this.schild += repair;
		}
		if(hülle) {
			this.hülle += repair;
		}
	}
	
	
	public void nachrichtAnAlle(String nachricht) {
		broadcastCommunicator.add(nachricht);
	}
	
	public static ArrayList<String> schreibelogbuch(){
		return broadcastCommunicator;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getEnergieversorgung() {
		return energieversorgung;
	}

	public void setEnergieversorgung(double energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	public double getSchild() {
		return schild;
	}

	public void setSchild(double schild) {
		this.schild = schild;
	}

	public double getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}

	public void setLebenserhaltungssysteme(double lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}

	public double getHülle() {
		return hülle;
	}

	public void setHülle(double hülle) {
		this.hülle = hülle;
	}

	public int getAnzahlPTorpedos() {
		return anzahlPTorpedos;
	}

	public void setAnzahlPTorpedos(int anzahlPTorpedos) {
		this.anzahlPTorpedos = anzahlPTorpedos;
	}

	public int getAnzahlAndroiden() {
		return anzahlAndroiden;
	}

	public void setAnzahlAndroiden(int anzahlAndroiden) {
		this.anzahlAndroiden = anzahlAndroiden;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public ArrayList<String> getBroadcastCommunicator() {
		return broadcastCommunicator;
	}

	public void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
		Raumschiff.broadcastCommunicator = broadcastCommunicator;
	}

}
