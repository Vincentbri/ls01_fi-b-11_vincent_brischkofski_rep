
public class Aufgabe1 {

	public static void main(String[] args) {
		String satz2 = "Beispiellsatz nummer 2 folgt";
		String stern = "**************";
	
		
	System.out.println("Das ist \"Beispiellsatz!\" nummer 1");
		
	System.out.printf("%20s\n", satz2) ;
	System.out.printf("\n");

		//println gibt den text in einen neuen absatz aus.
		//mehrere printf Anweisungen geben den text direkt hintereinander in der selben Zeile aus
	
	System.out.printf("%7.1s\n", stern);
	System.out.printf("%8.3s\n", stern);
	System.out.printf("%9.5s\n", stern);
	System.out.printf("%10.7s\n", stern);
	System.out.printf("%11.9s\n", stern);
	System.out.printf("%12.11s\n", stern);
	System.out.printf("%.13s\n", stern);
	System.out.printf("%8.3s\n", stern);
	System.out.printf("%8.3s\n", stern); 

	System.out.printf("\n");
	
	double d = 22.4234234;
	double e = 111.2222;
	double f = 4.0;
	double g = 1000000.551;
	double h = 97.34;
	
	System.out.printf("%.2f\n" ,d);
	System.out.printf("%.2f\n" ,e);
	System.out.printf("%.2f\n" ,f);
	System.out.printf("%.2f\n" ,g);
	System.out.printf("%.2f" ,h);


	
	}

}
